package com.whizdm.payment_service.customexceptions;

public class InvalidLoanId extends Exception{
    public InvalidLoanId(String message){
        super(message);
    }
}
